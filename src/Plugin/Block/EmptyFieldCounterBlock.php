<?php

namespace Drupal\empty_fields_counter\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Render\Markup;


/**
 * Provides a block with an empty field counter for the current node.
 *
 * @Block(
 *   id = "empty_field_counter_block",
 *   admin_label = @Translation("Empty Field Counter Block"),
 *   category = @Translation("Custom"),
 * )
 */
class EmptyFieldCounterBlock extends BlockBase implements ContainerFactoryPluginInterface
{

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs an EmptyFieldCounterBlock object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, RouteMatchInterface $route_match)
  {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    // Get the current node ID from the route parameters.
    $node_id = $this->routeMatch->getParameter('node');

    if ($node_id instanceof \Drupal\node\NodeInterface) {
      // Load the node.
      $node = $node_id;

      // Get the actual node ID.
      $node_id = $node->id();

      // Calculate the empty field count for the current node.
      $emptyFieldCount = $this->calculateEmptyFieldCount($node);
      $markup = Markup::create('<div class="progress"><div class="progress-done" data-done="' . $emptyFieldCount * 10 . '">' . $emptyFieldCount . '%</div></div>');

      // Build the output.
      $output = [
        '#markup' => $markup,
      ];
      $output['#cache']['contexts'][] = 'route';
      $output['#cache']['tags'][] = 'node:' . $node->id();
      return $output;
    }
  }


  /**
   * Calculates the count of empty fields for a given node.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node entity.
   *
   * @return int
   *   The count of empty fields.
   */
  protected function calculateEmptyFieldCount($node)
  {
    $emptyFieldCount = 0;

    // Loop through the fields and check if they are empty.
    foreach ($node->getFields() as $field_name => $field) {
      if ($field->isEmpty()) {
        $emptyFieldCount++;
      }
    }

    return $emptyFieldCount;
  }
}
